
import java.util.ArrayList;
import java.util.List;

public class CarroTest3 {

    public static void main(String[] args) {

        List<Carro> listaCarros = new ArrayList<>();
        Carro fusca = new Carro("Fusca", "Preto", 120);
        fusca.setPlaca("MPP3333");
        Carro opala = new Carro("Opala", "Azul", 150);
        Carro onix = new Carro("Onix", "Branco", 140);
        Carro santana = new Carro("Santana", "Verde", 160);
        santana.setPlaca("MPP3333");
        
        listaCarros.add(onix);
        listaCarros.add(fusca);
        listaCarros.add(opala);

        System.out.println("Sanatanas esta na lista ?" + listaCarros.contains(santana));
/*
        if (!listaCarros.contains(santana)) {
            listaCarros.add(santana);
        }

        for (Carro c : listaCarros) {
            System.out.println(c);
        }

*/

    }

}
