
public class Carro {

    private String cor;
    private String modelo;
    private int velocidadeMaxima;
    private String placa;
    
    private int velocidade;
    private boolean ligado;

    //construtor -> inicializa o meu objeto 
    public Carro(String modelo, String cor, int velocidadeMaxima) {
        this.modelo = modelo;
        this.cor = cor;
        this.velocidadeMaxima = velocidadeMaxima;
        this.ligado = false; 
        this.velocidade = 0 ; 

    }
    
    public String getModelo(){
        return this.modelo;
    }
    
    public String getCor(){
        return this.cor ; 
    }
    
    public void setCor(String cor){
        this.cor = cor ; 
    }
    
    public int getVelocidadeMaxima(){
       return  this.velocidadeMaxima ; 
    }
    
    public void setVelocidadeMaxima(int velocidadeMaxima){
        this.velocidadeMaxima = velocidadeMaxima ; 
    }
    
    public String getPlaca(){
        return this.placa ; 
    }
    
    public void setPlaca(String placa){
        this.placa = placa ; 
    }
    
    public boolean isLigado(){
        return this.ligado ; 
    }
    
    public int getVelocidade(){
        return this.velocidade ; 
    }
    
    public void ligar(){
        this.ligado = true ; 
    }
    
    public void desligar(){
        this.ligado = false  ; 
    }
    
    public void acelerar(){
        if(!this.ligado){
            throw new RuntimeException("Não é possivel Acelerar com o carro desligado") ; 
        }
        if(this.velocidade < this.velocidadeMaxima){
            this.velocidade += 10  ; 
        }
    }
    
    public void frear(){
        if(this.velocidade > 0) { 
            this.velocidade-= 10 ; 
        }
    }
    
    public String toString(){
        return "Modelo: " + this.modelo + "\n"
              + "Cor:" + this.cor + "\n" 
              + "Velocidade Maxima:" + this.velocidadeMaxima;
    }
    
    public boolean equals(Object object){
        
        if(object instanceof Carro){
             Carro c = (Carro) object ; 
             
            return this.placa.equals(c.getPlaca());
        }
        
        return false; 
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}
